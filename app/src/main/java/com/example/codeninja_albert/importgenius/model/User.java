package com.example.codeninja_albert.importgenius.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by codeninja-albert on 2/15/16.
 */
public class User {

    @SerializedName("user")
    private UserData userData;

    public String getFirstName()
    {
        return userData.getFirstname();
    }

    public String getUsername()
    {
        return userData.getUsername();
    }

    public int getUserId()
    {
        return userData.getUserId();
    }

    public String getStoken()
    {
        return userData.getStoken();
    }
}
