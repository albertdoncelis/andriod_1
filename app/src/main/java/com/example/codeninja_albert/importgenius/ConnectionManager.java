package com.example.codeninja_albert.importgenius;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by codeninja-albert on 2/15/16.
 */
public class ConnectionManager {

    private static RequestQueue requestQueue;

    public static RequestQueue getInstance(Context context)
    {
        if(requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context);
        }
        return requestQueue;
    }
}
