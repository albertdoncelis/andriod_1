package com.example.codeninja_albert.importgenius.request;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.example.codeninja_albert.importgenius.Config;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by codeninja-albert on 2/15/16.
 */
public class PHPRequest extends Request<String> {

    private Response.Listener<String> mListener;
    private Map<String,String> mParams = new HashMap<String, String>();

    public PHPRequest(Map<String, String> mParams, Response.Listener listener, Response.ErrorListener errorListener)
    {
        super(Method.POST, Config.URL_LOGIN_SCRUM, errorListener);
        this.mListener = listener;
        this.mParams = mParams;
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        String parsed;
        try {
            parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        } catch (UnsupportedEncodingException e) {
            parsed = new String(response.data);
        }
        return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
    }

    @Override
    protected void deliverResponse(String response) {
        if (mListener != null) {
            mListener.onResponse(response);
        }
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return mParams;
    }


}
