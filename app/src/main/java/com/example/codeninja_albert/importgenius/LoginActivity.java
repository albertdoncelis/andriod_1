package com.example.codeninja_albert.importgenius;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.codeninja_albert.importgenius.model.Message;
import com.example.codeninja_albert.importgenius.request.PHPRequest;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


    }

    /**
     *
     * @param view
     */
    public void signIn(View view)
    {
        String username;
        String password;
        EditText etUsername = (EditText) findViewById(R.id.username);
        EditText etPassword = (EditText) findViewById(R.id.password);
        username = etUsername.getText().toString();
        password = etPassword.getText().toString();
        Map<String, String> mParams = new HashMap<String ,String>();
        mParams.put("username",username);
        mParams.put("password",password);
        mParams.put("request","1");

        PHPRequest loginRequest = new PHPRequest(mParams, new LoginResponse(this.getApplicationContext()), new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        ConnectionManager.getInstance(this).add(loginRequest);
    }

    class LoginResponse implements Response.Listener<String>
    {
        private Context context;

        /**
         *
         * @param context
         */
        public LoginResponse(Context context)
        {
            this.context = context;
        }
        @Override
        public void onResponse(String response) {
            com.example.codeninja_albert.importgenius.model.Response success =
                    new Gson().fromJson(response,
                            com.example.codeninja_albert.importgenius.model.Response.class);
            Message message = new Gson().fromJson(response,Message.class);
            //login success
            success(success);

            errorMessage(
                    message ,
                    success,
                    context.getApplicationContext()
            );
        }

        /**
         *
         * @param success
         */
        private void success(com.example.codeninja_albert.importgenius.model.Response success)
        {
            if (success.getSuccess() == true) {

                Intent intent = new Intent(LoginActivity.this,DashBoardActivity.class);
                startActivity(intent);
                finish();
            }
        }

        /**
         *
         * @param message
         * @param success
         * @param context
         */
        private void errorMessage(Message message,
                com.example.codeninja_albert.importgenius.model.Response success,
                                  Context context)
        {
            if (success.getSuccess() == false) {
                Toast.makeText(context,message.getMessage().toString(),Toast.LENGTH_LONG).show();
            }
        }
    }
}
