package com.example.codeninja_albert.importgenius.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by codeninja-albert on 2/15/16.
 */
public class Message {

    @SerializedName("message")
    private String message;

    public String getMessage() {
        return message;
    }
}
