package com.example.codeninja_albert.importgenius;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;



/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {



    private View view;
    private ScrollView scrollView;
    private TableLayout searchTableLayout;
    private TableRow searchTable;
    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
         view = inflater.inflate(R.layout.fragment_search, container, false);

         scrollView = (ScrollView) view.findViewById(R.id.seach_scroll_container);


        addSearchTable();


        return view;
    }
    private void addSearchTable()
    {
        TableLayout searchTable  = (TableLayout) view.findViewById(R.id.search_table);
        LayoutInflater searchInflate = (LayoutInflater) getActivity().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        TableRow searchTableRow = (TableRow)  searchInflate.inflate(
                R.layout.search_table_row1,
                null);

        TableLayout.LayoutParams lp = new TableLayout.LayoutParams();

        lp.bottomMargin = 10;
        lp.topMargin    = 10;
        lp.leftMargin   = 30;
        lp.rightMargin  = 30;


        ImageView addNewSearchTable = (ImageView) searchTableRow.findViewById(R.id.add_search_container);
        addNewSearchTable.setOnClickListener(new AddSearchTable());

        ImageView removeThisSearchTable = (ImageView) searchTableRow.findViewById(R.id.remove_search_container);
        removeThisSearchTable.setOnClickListener(new RemoveSearchTable());

        if(searchTable.getChildCount() >= 1) {
            removeThisSearchTable.setVisibility(View.VISIBLE);
        }

        searchTable.addView(searchTableRow, lp);

    }

    class RemoveSearchTable implements View.OnClickListener
    {
        @Override
        public void onClick(View v) {
            searchTable = (TableRow) v.getParent().getParent().getParent();
             searchTableLayout = (TableLayout) searchTable.getParent();
//            ObjectAnimator animY = ObjectAnimator.ofFloat(searchTable, "translationY", -100f, 0f);


   //             searchTable.setLayoutTransition();
            ObjectAnimator animeAlpha = ObjectAnimator.ofFloat(searchTable, "alpha", 0f);
            ObjectAnimator animateLeft = ObjectAnimator.ofFloat(searchTable, "x", 1000f);
            ObjectAnimator animatedown = ObjectAnimator.ofFloat(searchTable, "y", 3000f);
            AnimatorSet animeAlphaLeft = new AnimatorSet();
            animeAlphaLeft.playTogether(animateLeft, animeAlpha, animatedown);

            animeAlphaLeft.setDuration(1000);//1sec
            animeAlphaLeft.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    searchTableLayout.removeView(searchTable);
                }
            });
            animeAlphaLeft.start();
//            //   searchTableLayout.removeView(searchTable);
        }
    }

    class AddSearchTable implements View.OnClickListener
    {
        @Override
        public void onClick(View view) {

            addSearchTable();
            scrollView.fullScroll(ScrollView.FOCUS_DOWN);
        }
    }

}
