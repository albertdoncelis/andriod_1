package com.example.codeninja_albert.importgenius.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by codeninja-albert on 2/15/16.
 */
public class UserData {

    @SerializedName("firstname")
    private String firstname;

    @SerializedName("username")
    private String username;

    @SerializedName("userid")
    private int userId;

    @SerializedName("stoken")
    private String stoken;

    public String getUsername()
    {
        return username;
    }

    public String getFirstname()
    {
        return firstname;
    }

    public int getUserId()
    {
        return userId;
    }

    public String getStoken()
    {
        return stoken;
    }

}
